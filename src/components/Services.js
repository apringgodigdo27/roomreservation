import React, { useState } from 'react'
import Title from './Title'
import FeaturedRooms from './FeaturedRooms'
import { FaGlassCheers, FaAirFreshener, FaBroom, FaBuilding } from 'react-icons/fa';

const Services = () => {

    const [services, setServices] = useState(
        [
            {
                icon: <FaAirFreshener />,
                title: 'Air Conditioner',
                info: "Ruangan dilengkapi dengan AC."
            },
            {
                icon: <FaBuilding />,
                title: 'Large Room',
                info: "Ruangan yang luas, dapat menampung sekitar 20 orang."
            },
            {
                icon: <FaBroom />,
                title: 'Clean Room',
                info: "Ruangan yang selalu bersih dan wangi."
            },
            {
                icon: <FaGlassCheers />,
                title: 'Free Food & Baverage',
                info: "Gratis makanan dan minuman."
            }

        ]
    )

    return (
        <>
            <section className="services">
                <Title title="services">
                        
                </Title>
                <div className="services-center">

                    {services.map((data, index) =>
                        <article key={index} className="service">
                            <span> {data.icon} </span>
                            <h6> {data.title} </h6>
                            <p> {data.info} </p>
                        </article>
                    )}

                </div>

            </section>

            <FeaturedRooms />
        </>
    )
}

export default Services
