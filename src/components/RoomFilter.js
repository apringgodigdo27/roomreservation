import React, { useContext } from 'react'
import { RoomContext } from '../Context'
import Title from './Title'

export default function RoomFilter({ rooms }) {
    const context = useContext(RoomContext)
    const { handleChange, price, minPrice, maxPrice, filterRooms } = context
    
    return (
        <section className="filter-container">
            <Title title="search rooms" />
            <form className="filter-form">
                {/* guest filter */}
                <div className="form-group">
                    <label htmlFor="type">Visitor</label>
                    <select
                        name="capacity"
                        id="capacity"
                        className="form-control"
                        onChange={handleChange}
                        onClick={filterRooms}
                    >
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                    </select>
                </div>
                {/* end guest filter */}
                {/* price filter */}
                <div className="form-group">
                    <label htmlFor="price">Room price ${price} </label>
                    <input type="range"
                        name="price"
                        min={minPrice}
                        max={maxPrice}
                        id="price"
                        value={price}
                        onChange={handleChange}
                        onClick={filterRooms}
                        className="form-control" />
                </div>
                {/* end price filter */}
            </form> 
        </section>
    )
}
