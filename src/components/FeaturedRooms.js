import React, { useContext } from "react";
import { RoomContext } from "../Context";
import Title from './Title'
import Loading from "./Loading";
import Room from './Room'
export default function FeaturedRooms() {

  let { loading, rooms: room } = useContext(RoomContext);
  
  const rooms = (room.map(room => {
    return <Room key={room.id} room={room}  />
  })
  )
  return (
    
    <section className="featured-rooms">
      <Title title="Rooms" />
      <div className="featured-rooms-center">
        {loading ? <Loading /> : rooms}
      </div>

    </section>
  );
}
