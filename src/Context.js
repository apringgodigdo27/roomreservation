import React, { useState, useEffect } from "react";
import items from "./data";

const RoomContext = React.createContext();

function RoomProvider({ children }) {

    const [rom, setRom] = useState({

        rooms: [],
        sortedRooms: [],
        featuredRooms: [],
        loading: true,
        //
        type: "all",
        capacity: 1,
        price: 0,
        minPrice: 0,
        maxPrice: 0,
        minSize: 0,
        maxSize: 0,
        breakfast: false,
        pets: false,
        name: ""
    })

    useEffect(() => {

        let rooms = formatData(items);
        let featuredRooms = rooms.filter(room => room.featured === true);
        let maxPrice = Math.max(...rooms.map(item => item.price));
        let maxSize = Math.max(...rooms.map(item => item.size));

        setRom(state => ({
            ...state,
            rooms,
            featuredRooms,
            sortedRooms: rooms,
            loading: false,
            price: maxPrice,
            maxPrice,
            maxSize
        }))

    }, [])

    const handleCheckBox = () => {
        setRom(state => ({
            ...state,
            breakfast: !state.breakfast
        }));

        let { rooms, breakfast } = rom;

        let tempRooms = [...rooms];

        if (breakfast) {

            tempRooms = tempRooms.filter(rooms => rooms.breakfast === true)
        }

        setRom(state => ({
            ...state,
            sortedRooms: tempRooms
        }))
    }

    const formatData = (ite) => {

        let tempItems = ite.map(item => {
            let id = item.sys.id
            let images = item.fields.images.map(image => image.fields.file.url)
            let room = { ...item.fields, images, id };
            return room;
        });

        return tempItems

    }

    const getRoom = slug => {
        let tempRooms = [...rom.rooms];
        const room = tempRooms.find(room => room.slug === slug)
        return room
    };


    const handleChange = (event) => {


        const target = event.target;
        const value = event.target.value;
        const name = target.name;

        setRom(state => ({
            ...state,
            [name]: value
        }
        ));

        filterRooms()
    };

    const filterRooms = () => {

        let {
            rooms,
            type,
            capacity,
            price,
            breakfast,
            pets
        } = rom;

        capacity = parseInt(capacity);

        let tempRooms = [...rooms];

        if (type !== "all") {
            tempRooms = tempRooms.filter(rooms => rooms.type === type)
        }
        if (capacity !== 1) {
            tempRooms = tempRooms.filter(rooms => rooms.capacity >= capacity)
        }

        tempRooms = tempRooms.filter(rooms => rooms.price <= price)

        if (breakfast) {
            tempRooms = tempRooms.filter(rooms => rooms.breakfast === true)
        }

        if (pets) {
            tempRooms = tempRooms.filter(rooms => rooms.pets === true)
        }

        setRom(state => ({
            ...state,
            sortedRooms: tempRooms
        }))
    }


    return (
        <RoomContext.Provider value={{ ...rom, getRoom, handleChange, filterRooms, handleCheckBox }}>
            {children}

        </RoomContext.Provider>
    )
}
const RoomConsumer = RoomContext.Consumer;

function withRoomConsumer(Component) {
    return function cosumerWrapper(props) {
        return <RoomConsumer>
            {value => <Component {...props} context={value} />}
        </RoomConsumer>
    }
}
export { RoomProvider, RoomConsumer, withRoomConsumer, RoomContext }
