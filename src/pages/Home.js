import React from 'react'
import Hero from '../components/Hero'
import Banner from '../components/Banner'
import Services from '../components/Services'
import { Link } from 'react-router-dom'

const Home = () => {
    return (
        <React.Fragment >
            <Hero >
                <Banner tittle="Alrescha Tower" subtitle="room meeting reservation tools">
                    <Link to="/rooms" className="btn-primary"> Show Room </Link>
                </Banner>
            </Hero>
            <Services />
        </React.Fragment >

    )
}

export default Home
