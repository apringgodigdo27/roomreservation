import room2 from "./images/room1-1.jpg";
import img1 from "./images/room1.jpg";
import img2 from "./images/room2.jpg";
import img3 from "./images/room3.jpg";

export default [
  {
    sys: {
      id: "1"
    },
    fields: {
      name: "Garuda Meeting Room",
      slug: "exclusive",
      type: "exclusive",
      price: 100,
      size: 200,
      capacity: 20,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices ex non mollis porta. Etiam molestie diam eu orci faucibus pulvinar. Aenean pellentesque scelerisque velit, ut varius lorem egestas ac. Ut eu sapien dui. Fusce accumsan ac lectus accumsan pellentesque. Sed malesuada velit eget urna pulvinar, vel congue purus congue. Nunc dignissim, dui at suscipit auctor, dolor neque luctus quam, eget eleifend neque leo feugiat dui. Etiam commodo consectetur pharetra. Curabitur elementum tincidunt rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae urna a enim efficitur feugiat non nec mi. Nulla viverra hendrerit risus nec vehicula. Praesent sagittis tincidunt nunc eu laoreet. Mauris vitae ipsum elit. In in fringilla lacus, nec gravida mauris. Nullam ac neque elit.",
      extras: [
        "AC",
        "Food & Baverage",
      ],
      images: [
        {
          fields: {
            file: {
              url: img1
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "2"
    },
    fields: {
      name: "Elang Meeting Room",
      slug: "premium",
      type: "premium",
      price: 150,
      size: 250,
      capacity: 10,
      pets: false,
      breakfast: false,
      featured: false,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices ex non mollis porta. Etiam molestie diam eu orci faucibus pulvinar. Aenean pellentesque scelerisque velit, ut varius lorem egestas ac. Ut eu sapien dui. Fusce accumsan ac lectus accumsan pellentesque. Sed malesuada velit eget urna pulvinar, vel congue purus congue. Nunc dignissim, dui at suscipit auctor, dolor neque luctus quam, eget eleifend neque leo feugiat dui. Etiam commodo consectetur pharetra. Curabitur elementum tincidunt rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae urna a enim efficitur feugiat non nec mi. Nulla viverra hendrerit risus nec vehicula. Praesent sagittis tincidunt nunc eu laoreet. Mauris vitae ipsum elit. In in fringilla lacus, nec gravida mauris. Nullam ac neque elit.",
        extras: [
          "AC",
          "Food & Baverage",
        ],
      images: [
        {
          fields: {
            file: {
              url: img2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "3"
    },
    fields: {
      name: "single standard",
      slug: "standard",
      type: "standard",
      price: 50,
      size: 300,
      capacity: 5,
      pets: true,
      breakfast: false,
      featured: false,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices ex non mollis porta. Etiam molestie diam eu orci faucibus pulvinar. Aenean pellentesque scelerisque velit, ut varius lorem egestas ac. Ut eu sapien dui. Fusce accumsan ac lectus accumsan pellentesque. Sed malesuada velit eget urna pulvinar, vel congue purus congue. Nunc dignissim, dui at suscipit auctor, dolor neque luctus quam, eget eleifend neque leo feugiat dui. Etiam commodo consectetur pharetra. Curabitur elementum tincidunt rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae urna a enim efficitur feugiat non nec mi. Nulla viverra hendrerit risus nec vehicula. Praesent sagittis tincidunt nunc eu laoreet. Mauris vitae ipsum elit. In in fringilla lacus, nec gravida mauris. Nullam ac neque elit.",
        extras: [
          "AC",
          "Food & Baverage",
        ],
      images: [
        {
          fields: {
            file: {
              url: img3
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        },
        {
          fields: {
            file: {
              url: room2
            }
          }
        }
      ]
    }
  },
];
